package com.Stepdefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {
	WebDriver driver;
	@Given("open the browser")
	public void open_the_browser() {
		driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("https://demowebshop.tricentis.com/login");
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);  
	}

	@When("user should enter valid {string} in email textbox")
	public void user_should_enter_valid_in_email_textbox(String string) {
		 driver.findElement(By.id("Email")).sendKeys(string);

	}

	@When("user should enter valid {string} in password textbox")
	public void user_should_enter_valid_in_password_textbox(String string) {
		 driver.findElement(By.id("Password")).sendKeys(string);

	}

	@Then("user should click login button")
	public void user_should_click_login_button() {
		 driver.findElement(By.xpath("//input[@value='Log in']")).click();
         driver.quit();
	}


}
