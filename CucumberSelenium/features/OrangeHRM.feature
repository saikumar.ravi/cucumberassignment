Feature: user should able to login with valid credentials

  Scenario Outline: verify LoginPage of Orange HRM
    Given launch the browser
    When user should enter valid <username1> in email textbox
    And user should enter valid <Password1> in password textbox
    Then User should click login button

    Examples: 
      | username | Password   |
      | "Admin"  | "admin123" |
      | "Admin"  | "admin123" |
