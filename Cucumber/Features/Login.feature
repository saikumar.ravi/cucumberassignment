Feature: user should able to login with valid credentials

Scenario: verify LoginPage
Given open the browser 
Then the homepage of website is get displayed
When user should enter valid username in email textbox
And user should enter valid Password in password textbox
And user should click login button
Then the user should taken to homepage of demowebshop
And close the browser
