package stepDefiniton;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Demologin {
	WebDriver driver;
	@Given("open the browser")
	public void open_the_browser() {
		 driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("https://demowebshop.tricentis.com/login");
         driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	} 
	@Then("the homepage of website is get displayed")
	public void the_homepage_of_website_is_get_displayed() {

		System.out.println("the homepage is displayed");
	}
	@When("user should enter valid username in email textbox")
	public void user_should_enter_valid_username_in_email_textbox() {
		 driver.findElement(By.id("Email")).sendKeys("ravisaikumar97@gmail.com");

	}
	@When("user should enter valid Password in password textbox")
	public void user_should_enter_valid_password_in_password_textbox() {
		 driver.findElement(By.id("Password")).sendKeys("Sai@1234");

	}
	@When("user should click login button")
	public void user_should_click_login_button() {
		 driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}
	@Then("the user should taken to homepage of demowebshop")
	public void the_user_should_taken_to_homepage_of_demowebshop() {
		System.out.println("The homepage of demowebshop is shown");
	}
	@Then("close the browser")
	public void close_the_browser() {
		driver.quit();
	}
}
